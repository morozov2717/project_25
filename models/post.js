const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema({
    genre: { type: String, required: true },
    name: { type: String, required: true },
    description: { type: String, required: true },
});

const Post = mongoose.model('Post', postSchema);

module.exports = Post;
